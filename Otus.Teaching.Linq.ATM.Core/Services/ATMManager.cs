﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System.Text.Json;


namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата

        public User GetUserInfo(string login, string password)
        {
            return Users.FirstOrDefault(x => x.Login == login && x.Password == password);
        }

        public string GetAllUserInfo(int userID)
        {
            var userAllInfo = (from allinfo in Users
                            where allinfo.Id == userID
                            select allinfo);

            return JsonSerializer.Serialize(userAllInfo, new JsonSerializerOptions() { WriteIndented = true });
        }

        public string GetUsersAcc(int userID)
        {
            var usersAccount = (from account in Accounts
                                join user in Users on account.UserId equals user.Id into newTable
                                from itemT in newTable
                                where itemT.Id == userID
                                select account);

            return JsonSerializer.Serialize(usersAccount, new JsonSerializerOptions() { WriteIndented = true });
        }

        public string GetUsersAccHistory(int userID)
        {
            var usersAccountAndHistory = (from acc in Accounts
                                          join hst in History on acc.Id equals hst.AccountId
                                          where acc.UserId == userID
                                          group hst by hst.AccountId into g
                                          select new
                                          {
                                              Account = g.Key,
                                              hist = from h in g select h
                                          }
                                            );

            return JsonSerializer.Serialize(usersAccountAndHistory, new JsonSerializerOptions() { WriteIndented = true });
        }

        public string GetInfoIncome()
        {
            var infoIncome = (from user in Users
                              join account in Accounts on user.Id equals account.UserId
                              orderby user.Id
                              select new
                              {
                                  user.FirstName,
                                  user.MiddleName,
                                  user.SurName,
                                  user.Phone,
                                  user.RegistrationDate,
                                  user.PassportSeriesAndNumber,
                                  account.Id
                              }).GroupJoin(
                                                                                      History.Where(x => x.OperationType == OperationType.InputCash),
                                                                                      user => user.Id,
                                                                                      history => history.AccountId,
                                                                                      (user, history) => new {
                                                                                          user,
                                                                                          history
                                                                                      });
            return JsonSerializer.Serialize(infoIncome, new JsonSerializerOptions() { WriteIndented = true });
        }

        public string GetInfoUserMoney(decimal n)
        {
            var infoUserWithMoney = (from
                                      account in Accounts.Where(x => x.CashAll > n)
                                     group account by account.UserId into g
                                     join user in Users on g.Key equals user.Id
                                     select new { user.Login, user.FirstName, user.SurName, user.PassportSeriesAndNumber, user.Phone, user.RegistrationDate });

            return JsonSerializer.Serialize(infoUserWithMoney, new JsonSerializerOptions() { WriteIndented = true });
        }
    }
}