﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    using System;
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов

            while (true)
            {
                System.Console.WriteLine("Для авторизации введите login. Для выхода введите exit");
                System.Console.WriteLine("Для вывода всех пополнений счета пользователей введите income");
                System.Console.WriteLine("Для вывода пользователей с указанным остатком и выше на счёте введите sum");
                string authorization = Console.ReadLine();
                switch (authorization)
                {
                    case "login":
                        ActionByLogin(atmManager);
                        break;
                    case "exit":
                        System.Console.WriteLine("Завершение работы приложения-банкомата...");
                        return;
                    case "income":
                        Console.WriteLine(atmManager.GetInfoIncome());
                        break;
                    case "sum":
                        Console.WriteLine("Введите сумму:");
                    try
                    {
                        Console.WriteLine(atmManager.GetInfoUserMoney(Convert.ToDecimal(Console.ReadLine())));
                    }
                    catch
                    {
                        Console.WriteLine("Некорректно введена сумма!");
                    }
                    break;

                default:
                        Console.WriteLine("Неверная команда!");
                        break;
                }   

                
            }

        }

        static void ActionByLogin (ATMManager atm)
        {
            Console.WriteLine("Введите ваш логин:");
            string login = Console.ReadLine();

            Console.WriteLine("Введите ваш пароль:");
            string pwd = Console.ReadLine();

            var userinfo = atm.GetUserInfo(login, pwd);
            Console.WriteLine($"Информация о пользователе:{login}");
            Console.WriteLine(userinfo.ToString());
            Console.WriteLine(atm.GetAllUserInfo(userinfo.Id));            

            while (true)
            {
                Console.WriteLine($"Для получения всех счетов пользователя '{login}' введите: useracc");
                Console.WriteLine($"Для получения всех счетов пользователя, включая историю по каждому счёту '{login}' введите: useroper");
                Console.WriteLine("Для выхода введите exit");
                string info = Console.ReadLine();
                switch (info)
                {
                    case "useracc":
                        Console.WriteLine(atm.GetUsersAcc(userinfo.Id));
                        break;
                    case "useroper":
                        Console.WriteLine(atm.GetUsersAccHistory(userinfo.Id));
                        break;
                    default:
                        Console.WriteLine("Неверная команда!");
                        break;
                    case "exit":
                        System.Console.WriteLine("Завершение работы приложения-банкомата...");
                        return;

                }
            }
            
            
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}